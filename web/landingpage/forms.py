from django import forms

class EmailForm(forms.Form):
    email = forms.EmailField(label="", widget=forms.TextInput(attrs={'class': 'form-control input-lg'}))
