from django.contrib import admin
from .models import EmailRecord

admin.site.register(EmailRecord)
