from django.template import loader
from django.shortcuts import render
from django.views.generic.base import View

from .forms import EmailForm
from .models import EmailRecord

class MainView(View):
    def get(self, request):
        return render(request, 'landingpage/index.html', {'form': EmailForm()})

    def post(self, request):
            form = EmailForm(request.POST)

            record = EmailRecord()
            record.email = form['email'].value()
            record.save()

            return render(request, 'landingpage/index.html', {'form': EmailForm(), 'thanks_message': "Thank you! We'll keep you in touch."})
